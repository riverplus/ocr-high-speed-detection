﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViDi2;
using ViDi2.Local;
using ViDi2.Runtime.Local;
using ViDi2.UI;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using Encoder = System.Drawing.Imaging.Encoder;
using Exception = System.Exception;

namespace Nestle.DL.Runtime.Blue
{
    public partial class Form1 : Form
    {
        // For ViDi Suite 
        private IWorkspace workspace;
        private IStream stream;
        private SampleViewer sampleViewer;
        private ViDi2.Runtime.Local.Control control;
        private Stopwatch Stopwatch = new Stopwatch();
        //Store Image List 
        List<string> images = new List<string>();
        int index = 0;

        //Mode false = Camera, true = Simmulate
        bool modeSimmulate = false;
 
        //Timer 
        private Timer timer1 = new Timer();
        //Count If processing time > 35 ms
        int count = 0;
        int totalrun = 0, goodrun =0, badrun =0;
        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public Form1()
        {
            // initiallize ViDi UI
            if (!ViDi2.UI.ViDiSuiteServiceLocator.IsInitialized) ViDi2.UI.ViDiSuiteServiceLocator.Initialize();

            InitializeComponent();
        }
        // Load ImageFile Path
        private void LoadImageBT_Click(object sender, EventArgs e)
        {
            
           
            //var fd = new System.Windows.Forms.FolderBrowserDialog();
            //if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{

            //    for(int i = 0; i < Directory.GetFiles(fd.SelectedPath).Length; i++)
            //    {
            //        images.Add(fd.SelectedPath + '/' + Path.GetFileName(Directory.GetFiles(fd.SelectedPath)[i]));
            //    }
            //}
            //LoadImagePath.Text = fd.SelectedPath.ToString();
        }
        //Run WorkSpace 1 Time
        private void RunOnceBT_Click(object sender, EventArgs e)
        {
            runWorkspace();
        }

        private void ContinuousCB_CheckedChanged(object sender, EventArgs e)
        {
            if (ContinuousCB.Checked)
            {
                RunOnceBT.Enabled = false;
                timer1.Start();
            }
            else
            {
                RunOnceBT.Enabled = true;
                timer1.Stop();
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            runWorkspace();
        }
    
        private void runWorkspace()
        {
            string temp = "";
            try
            {
                if(index > images.Count()-1)
                {
                    index = 0;
                }
                using (IImage image = new LibraryImage(images[index]))
                {
                    Stopwatch.Start();
                    sampleViewer.Sample = stream.Process(image);
                    IBlueMarking BlueMarking = stream.Process(image).Markings[sampleViewer.ToolName] as IBlueMarking;
                    
                    //IImage img1 = BlueMarking.ViewImage(index);
                    if (chkRecord.Checked)
                    {
                        ImageRecord(images[index]);
                    }
                    //Count Number of OCRStopwatch.Stop();
                    Stopwatch.Stop();
                    if (chkData.Checked)
                    {
                        dataGridView1.Rows.Add(DateTime.Now.ToString().Replace('/', '-').Replace(':', '-').Replace(' ', '_').Replace('.', '-'),Convert.ToInt32( Stopwatch.ElapsedMilliseconds));
                    }
                    label3.Text = BlueMarking.Views[0].Features.Count().ToString();
                    for(int i=0; i<BlueMarking.Views[0].Features.Count;i++)
                    {
                        temp += BlueMarking.Views[0].Features[i].Id.ToString();
                    }
                    txtResults.Text = temp;
                   
                    ProcessingText.Text = (Stopwatch.ElapsedMilliseconds).ToString() + " msec";
                    
                        if ((Stopwatch.ElapsedMilliseconds) >= 35)
                       {
                            count = count + 1;
                            OverPS.Text = count.ToString();
                       }

                   
                    index = index + 1;
                    totalrun = totalrun + 1;
                    Stopwatch.Reset();
                    label6.Text = totalrun.ToString();
                }
            }
            catch (InvalidCastException e)
            {
                MessageBox.Show("Can't Load Image Files");
            }
        }
        private void ImageRecord(string filename)
        {
            Image bitmap = Image.FromFile(filename);
            bitmap.Save(@"D:\ResultImage\"+DateTime.Now.ToString().Replace(':','-').Replace('/','-').Replace('.','-').Replace(' ','_')+".bmp");
        }
        private void DataRecord(int processingTime)
        {
            string constr = ConfigurationManager.ConnectionStrings["ConnectionString_DB"].ConnectionString;
            SqlConnection connection = new SqlConnection(constr);
            string str_insert = "INSERT INTO(TimeStamp,Result,ProcessingTime) VALUES(@stamp,@res,@process)";
            SqlCommand command = new SqlCommand(str_insert, connection);
            command.Parameters.AddWithValue("@stamp",DateTime.Now);
            command.Parameters.AddWithValue("@res","NA");
            command.Parameters.AddWithValue("@process", processingTime);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
               // connection.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"you cann't insert date to datacase");
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        string PathImage;
        private void button1_Click(object sender, EventArgs e)
        {
            var fd = new System.Windows.Forms.FolderBrowserDialog();
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PathImage = fd.SelectedPath;
            }
            //lbImagRec.Text = fd.SelectedPath.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportData();
        }
        private void ExportData()
        {
            if (dataGridView1.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "CSV (*.csv)|*.csv";
                sfd.FileName =DateTime.Now.ToString().Replace(':', '-').Replace('/', '-').Replace('.', '-').Replace(' ', '_') + ".csv";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            int columnCount = dataGridView1.Columns.Count;
                            string columnNames = "";
                            string[] outputCsv = new string[dataGridView1.Rows.Count + 1];
                            for (int i = 0; i < columnCount; i++)
                            {
                                columnNames += dataGridView1.Columns[i].HeaderText.ToString() + ",";
                            }
                            outputCsv[0] += columnNames;

                            for (int i = 1; i < dataGridView1.Rows.Count; i++)
                            {
                                for (int j = 0; j < columnCount; j++)
                                {
                                    outputCsv[i] += dataGridView1.Rows[i - 1].Cells[j].Value.ToString() + ",";
                                }
                            }

                            File.WriteAllLines(sfd.FileName, outputCsv, Encoding.UTF8);
                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }

        private void directoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.InitialDirectory = @"..\ocr-high-speed-detection";
            fd.Filter = "workspace files (*.vrws)|*.vrws|All files (*.*)|*.*";

            if (fd.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.WorkspacePath = fd.FileName;
                Properties.Settings.Default.Save();
                try
                {
                    // create SampleView for display VPDL image
                    sampleViewer = new SampleViewer();
                    elementHost1.Child = sampleViewer as System.Windows.Controls.UserControl;
                    // create control for workspace management
                    control = new ViDi2.Runtime.Local.Control(GpuMode.Deferred);
                    control.InitializeComputeDevices(GpuMode.SingleDevicePerTool, new List<int>() { });
                    // Load Runtime 

                    workspace = control.Workspaces.Add("workspace", Properties.Settings.Default.WorkspacePath);
                    stream = workspace.Streams["default"];
                    directoryToolStripMenuItem.BackgroundImage = Properties.Resources.Correct;
                    StatusLabel1.Text = Properties.Settings.Default.WorkspacePath;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Workspace Error");
                }
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //timer1.Tick += new EventHandler(timer1_Tick); // Everytime timer ticks, timer_Tick will be called
            //timer1.Interval = (100);              // Timer will tick evert 100ms
            //timer1.Enabled = true;                       // Enable the timer
            
        }

        private void imagePathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.ImagePath = fd.SelectedPath;
                for (int i = 0; i < Directory.GetFiles(fd.SelectedPath).Length; i++)
                {
                    images.Add(fd.SelectedPath + '/' + Path.GetFileName(Directory.GetFiles(fd.SelectedPath)[i]));
                }
                StatusLabel1.Text = Properties.Settings.Default.ImagePath;
            }

        }

        private void enableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(Properties.Settings.Default.ImagePath))
            {
                modeSimmulate = true;
                enableToolStripMenuItem.Image = Properties.Resources.Correct;
                enableToolStripMenuItem1.Image = Properties.Resources.Wrong;
            }
            else
            {
                MessageBox.Show("Please check image path again.","Not found path");
            }
        }

        private void enableToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            modeSimmulate = false;
            enableToolStripMenuItem.Image = Properties.Resources.Wrong;
            enableToolStripMenuItem1.Image = Properties.Resources.Correct;
        }
    }
    }

