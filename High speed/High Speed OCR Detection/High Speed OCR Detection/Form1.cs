﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cognex.VisionPro;
using Cognex.VisionPro.Exceptions;
using ViDi2;
using ViDi2.Local;
using ViDi2.Runtime.Local;
using System.Diagnostics;
using System.IO;
using ViDi2.UI;
using System.Drawing.Imaging;
using Encoder = System.Drawing.Imaging.Encoder;
using Exception = System.Exception;

namespace High_Speed_OCR_Detection
{
    public partial class Form1 : Form
    {
        private ICogAcqFifo mAcqFifo = null;
        private ICogFrameGrabber mFrameGrabber = null;
        private int numAcqs = 0;
        private CogAcqFifoTool mTool;
        private ICogAcqBrightness mBrightness;
        private ICogAcqContrast mContrast;
        private ICogAcqExposure mExposure;
        private bool StopAcquire;
        // For ViDi Suite 
        private IWorkspace workspace;
        private IStream stream;
        private ViDi2.UI.SampleViewer sampleViewer;
        private ViDi2.Runtime.Local.Control control;
        private Stopwatch Stopwatch = new Stopwatch();
        public Form1()
        {
            if (!ViDi2.UI.ViDiSuiteServiceLocator.IsInitialized) ViDi2.UI.ViDiSuiteServiceLocator.Initialize();
            InitializeComponent();
            
        }

        private void InitializeAcquisition()
        {
            try
            {
                mTool = new CogAcqFifoTool();
                //mAcqFifo.FrameGrabber
                // Step 1 - Create the CogFrameGrabbers
                CogFrameGrabbers mFrameGrabbers = new CogFrameGrabbers();
                if (mFrameGrabbers.Count < 1)
                    throw new CogAcqNoFrameGrabberException("No frame grabbers found");

                mAcqFifo = mTool.Operator;
                mAcqFifo.Connect(mFrameGrabbers[0]);
                mContrast = mTool.Operator.OwnedContrastParams;
                mBrightness = mTool.Operator.OwnedBrightnessParams;
                mExposure = mTool.Operator.OwnedExposureParams;
    
                // Display the board type
                lblBoardType.Text = mAcqFifo.FrameGrabber.Name;
                lblVideoFormat.Text = mTool.Operator.VideoFormat;
                
                
                //AcquireButton.Enabled = false;
                numContrast.Value = Convert.ToDecimal( mContrast.Contrast);
                numBrightness.Value = Convert.ToDecimal(mBrightness.Brightness);
                numExposure.Value = Convert.ToDecimal(mExposure.Exposure);
                sampleViewer = new SampleViewer();
                elementHost1.Child = sampleViewer as System.Windows.Controls.UserControl;

            }
            catch (CogAcqException ex)
            {
                MessageBox.Show("No camera is connected " + ex.Message);
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                InitializeAcquisition();
            }
            catch (CogException ex)
            {
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
            catch (System.Exception gex)
            {
                MessageBox.Show(gex.Message);
                Application.Exit();
            }
        }



        private void AcquireButton_Click(object sender, EventArgs e)
        {
            int trignum;
            if (!chkSim.Checked)
            {
                if (mAcqFifo != null)
                {
                    // Step 4: Acquire an image
                    cogDisplay1.Image = mAcqFifo.Acquire(out trignum);
                    numAcqs++;
                    if (numAcqs > 4)
                    {
                        GC.Collect();
                        numAcqs = 0;
                    }
                }
            }
            else
            {
                runWorkspace(null);
            }
        }

        private void numExposure_ValueChanged(object sender, EventArgs e)
        {
            mTool.Operator.OwnedExposureParams.Exposure = Convert.ToDouble(numExposure.Value);
            AcquireButton_Click(null, null);
        }

        private void btn_Live_Click(object sender, EventArgs e)
        {
            if (cogDisplay1.LiveDisplayRunning)
            {
                // Step 2 - Stop the live display
                cogDisplay1.StopLiveDisplay();
                btn_Live.Text = "Live";
                AcquireButton.Enabled = true;
            }
            else
            {
                // Step 3 - Start the live display
                cogDisplay1.StartLiveDisplay(mTool.Operator);
                btn_Live.Text = "Stop";
                AcquireButton.Enabled = false;
            }
        }

        private void mManual_CheckedChanged(object sender, EventArgs e)
        {
            StopAcquire = true;
            mAcqFifo.OwnedTriggerParams.TriggerEnabled = false;
            mTool.Operator.Flush();
            // Step 3 - Set manual trigger mode
            mAcqFifo.OwnedTriggerParams.TriggerModel = CogAcqTriggerModelConstants.Manual;
            mAcqFifo.OwnedTriggerParams.TriggerEnabled = true;
            AcquireButton.Enabled = true;
        }

        private void mAuto_CheckedChanged(object sender, EventArgs e)
        {
            StopAcquire = false;
            mAcqFifo.OwnedTriggerParams.TriggerEnabled = false;
            mTool.Operator.Flush();
            // Step 3 - Set manual trigger mode
            mAcqFifo.OwnedTriggerParams.TriggerModel = CogAcqTriggerModelConstants.Auto;
            mAcqFifo.OwnedTriggerParams.TriggerEnabled = true;
            AcquireButton.Enabled = false;
            mTool.Operator.Complete += new CogCompleteEventHandler(Acq_Complete);
            
        }
        private void Acq_Complete(object sender, CogCompleteEventArgs e)
        {
            if (InvokeRequired)
            {
                var eventArgs = new object[] { sender, e };
                Invoke(new CogCompleteEventHandler(Acq_Complete), eventArgs);
                return;
            }

            try
            {
                if (StopAcquire)
                {
                    return;
                }
                // Retrieve an image if it is available.
                var info = new CogAcqInfo();
                //mTool.Operator.GetFifoState(
                //if (numReadyVal > 0)
                //{
                cogDisplay1.Image = mTool.Operator.CompleteAcquireEx(info);
                Bitmap img = mTool.Operator.CompleteAcquireEx(info).ToBitmap();
                //pictureBox2.Image = img;
                runWorkspace(img);
                numAcqs += 1;
                //}
                //else
                //{
                //    // TO DO: Add error handling code here.
                //}
                // Queue another acquisition request if we are in manual trigger mode
                if (mManual.Checked)
                {
                    mTool.Operator.StartAcquire();
                }
                // We need to run the garbage collector on occasion to cleanup
                // images that are no longer being used.
                if (numAcqs > 4)
                {
                    GC.Collect();
                    numAcqs = 0;
                }
            }
            catch (CogException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnWorkspace_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.InitialDirectory = @"..\ocr-high-speed-detection";
            fd.Filter = "workspace files (*.vrws)|*.vrws|All files (*.*)|*.*";

            if (fd.ShowDialog() == DialogResult.OK)
            {
                //Properties.Settings.Default.WorkspacePath = fd.FileName;
                //Properties.Settings.Default.Save();
                try
                {
                    // create SampleView for display VPDL image
                    sampleViewer = new SampleViewer();
                    elementHost1.Child = sampleViewer as System.Windows.Controls.UserControl;
                    // create control for workspace management
                    control = new ViDi2.Runtime.Local.Control(GpuMode.Deferred);
                    control.InitializeComputeDevices(GpuMode.SingleDevicePerTool, new List<int>() { });
                    // Load Runtime 

                    workspace = control.Workspaces.Add("workspace", fd.FileName);
                    stream = workspace.Streams["default"];
                    txtWorkspace.Text = fd.FileName;
                    MessageBox.Show("Workspace Done","Complete");

                    //directoryToolStripMenuItem.BackgroundImage = Properties.Resources.Correct;
                    //StatusLabel1.Text = Properties.Settings.Default.WorkspacePath;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Workspace Error");
                }
            }
        }
        int index = 0;
        int countPass = 0, countFail = 0, countTotal = 0;

        private void btnReset_Click(object sender, EventArgs e)
        {
            countPass = 0;
            countFail = 0;
            countTotal = 0;
            txtGood.Text = countPass.ToString();
            txtBad.Text = countFail.ToString();
            txtTotal.Text = countTotal.ToString();
        }

        private void btnImage_Click(object sender, EventArgs e)
        {
            var fd = new System.Windows.Forms.FolderBrowserDialog();
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                for (int i = 0; i < Directory.GetFiles(fd.SelectedPath).Length; i++)
                {
                    images_sim.Add(fd.SelectedPath + '/' + Path.GetFileName(Directory.GetFiles(fd.SelectedPath)[i]));
                }
            }
            lbImagePath.Text = fd.SelectedPath.ToString();
        }

        List<string> images_sim = new List<string>();
        private void runWorkspace(Bitmap img)
        {
            int count = 0;
            string temp = "";
            try
            {
                if (!chkSim.Checked)
                {
                    using (IImage image = new ViDi2.FormsImage(img))
                    {
                        Stopwatch.Start();
                        //sampleViewer.
                        //ViDi2.UI.ISampleViewerView = new 
                        //ViDi2.ISample sample = stream.Process(image);

                        sampleViewer.Sample = stream.Process(image);
                        IBlueMarking BlueMarking = stream.Process(image).Markings["Read"] as IBlueMarking;

                        //IImage img1 = BlueMarking.ViewImage(index);

                        //Count Number of OCRStopwatch.Stop();
                        
                        txtCount.Text = BlueMarking.Views[0].Features.Count().ToString();
                        for (int i = 0; i < BlueMarking.Views[0].Features.Count; i++)
                        {
                            temp += BlueMarking.Views[0].Features[i].Id.ToString();
                        }
                        txtResults.Text = temp;

                        ProcessingText.Text = (Stopwatch.ElapsedMilliseconds).ToString() + " msec";

                        if ((Stopwatch.ElapsedMilliseconds) >= 35)
                        {
                            count++;
                            OverPS.Text = count.ToString();
                        }
                        if ((int)numValidate.Value == BlueMarking.Views[0].Features.Count())
                        {
                            countPass++;
                            pictureBox2.Image = Properties.Resources.correcto_incorrecto_png_7_Transparent_Images;
                        }
                        else
                        {
                            countFail++;
                            pictureBox2.Image = Properties.Resources.icon_silang_png_Transparent_Images;
                        }
                        Stopwatch.Stop();
                        countTotal = countPass + countFail;
                        txtGood.Text = countPass.ToString();
                        txtBad.Text = countFail.ToString();
                        txtTotal.Text = countTotal.ToString();
                        Stopwatch.Reset();
                    }
                }
                else
                {
                    if (index > images_sim.Count() - 1)
                    {
                        index = 0;
                    }
                    using (IImage image = new LibraryImage(images_sim[index]))
                    {
                        Stopwatch.Start();
                        sampleViewer.Sample = stream.Process(image);
                        
                        IBlueMarking BlueMarking = stream.Process(image).Markings[sampleViewer.ToolName] as IBlueMarking;
                        //Count Number of OCR
                        txtCount.Text = BlueMarking.Views[0].Features.Count().ToString();
                        for (int i = 0; i < BlueMarking.Views[0].Features.Count; i++)
                        {
                            temp += BlueMarking.Views[0].Features[i].Id.ToString();
                        }
                        txtResults.Text = temp;
                        Stopwatch.Stop();
                        ProcessingText.Text = (Stopwatch.ElapsedMilliseconds).ToString() + " msec";

                        if ((Stopwatch.ElapsedMilliseconds) >= 35)
                        {
                            count = count + 1;
                            OverPS.Text = count.ToString();
                        }
                        Stopwatch.Reset();
                    }
                }
            }
            catch (InvalidCastException e)
            {
                MessageBox.Show("Can't Load Image Files");
            }
        }

    }
}
