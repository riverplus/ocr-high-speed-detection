﻿
namespace Nestle.DL.Runtime.Blue
{
    partial class frmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nest1200ppmDBDataSet = new Nestle.DL.Runtime.Blue.Nest1200ppmDBDataSet();
            this.tbHistoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tb_HistoryTableAdapter = new Nestle.DL.Runtime.Blue.Nest1200ppmDBDataSetTableAdapters.Tb_HistoryTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.nest1200ppmDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHistoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nest1200ppmDBDataSet
            // 
            this.nest1200ppmDBDataSet.DataSetName = "Nest1200ppmDBDataSet";
            this.nest1200ppmDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbHistoryBindingSource
            // 
            this.tbHistoryBindingSource.DataMember = "Tb_History";
            this.tbHistoryBindingSource.DataSource = this.nest1200ppmDBDataSet;
            // 
            // tb_HistoryTableAdapter
            // 
            this.tb_HistoryTableAdapter.ClearBeforeFill = true;
            // 
            // frmReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "frmReport";
            this.Text = "frmReport";
            this.Load += new System.EventHandler(this.frmReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nest1200ppmDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHistoryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Nest1200ppmDBDataSet nest1200ppmDBDataSet;
        private System.Windows.Forms.BindingSource tbHistoryBindingSource;
        private Nest1200ppmDBDataSetTableAdapters.Tb_HistoryTableAdapter tb_HistoryTableAdapter;
    }
}