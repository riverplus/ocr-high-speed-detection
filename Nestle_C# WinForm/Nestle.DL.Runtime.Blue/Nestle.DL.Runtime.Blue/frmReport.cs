﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nestle.DL.Runtime.Blue
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nest1200ppmDBDataSet.Tb_History' table. You can move, or remove it, as needed.
            this.tb_HistoryTableAdapter.Fill(this.nest1200ppmDBDataSet.Tb_History);

        }
    }
}
