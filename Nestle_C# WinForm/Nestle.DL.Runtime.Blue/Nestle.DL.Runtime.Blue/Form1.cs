﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViDi2;
using ViDi2.Local;
using ViDi2.Runtime.Local;
using ViDi2.UI;
using System.Data.SqlClient;
using System.Configuration;

namespace Nestle.DL.Runtime.Blue
{
    public partial class Form1 : Form
    {
        // For ViDi Suite 
        private IWorkspace workspace;
        private IStream stream;
        private SampleViewer sampleViewer;
        private ViDi2.Runtime.Local.Control control;
        private Stopwatch Stopwatch = new Stopwatch();
        //Store Image List 
        List<string> images = new List<string>();
        int index = 0;
 
        //Timer 
        private Timer timer1 = new Timer();
        //Count If processing time > 35 ms
        int count = 0;
        int totalrun = 0;
        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public Form1()
        {
            // initiallize ViDi UI
            if (!ViDi2.UI.ViDiSuiteServiceLocator.IsInitialized) ViDi2.UI.ViDiSuiteServiceLocator.Initialize();

            InitializeComponent();
            timer1.Tick += new EventHandler(timer1_Tick); // Everytime timer ticks, timer_Tick will be called
            timer1.Interval = (100);              // Timer will tick evert 100ms
            //timer1.Enabled = true;                       // Enable the timer
            

            // create SampleView for display VPDL image
            sampleViewer = new SampleViewer();
            elementHost1.Child = sampleViewer as System.Windows.Controls.UserControl;
            // create control for workspace management
            control = new ViDi2.Runtime.Local.Control(GpuMode.Deferred);
            control.InitializeComputeDevices(GpuMode.SingleDevicePerTool, new List<int>() { });
            // Load Runtime 
            workspace = control.Workspaces.Add("workspace", @"D:\Git Project\ocr-high-speed-detection\ViDiSubTask_ROI.vrws");
            stream = workspace.Streams["default"];
        }
            // Load ImageFile Path
        private void LoadImageBT_Click(object sender, EventArgs e)
        {

            if (LoadImagePath.Text.Length > 0)
            {
                var fd = new System.Windows.Forms.FolderBrowserDialog();
                //fd.RootFolder = @"D:\Presale\2021\PS19715 Nestle Nawanakron\OK Image";
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    for (int i = 0; i < Directory.GetFiles(fd.SelectedPath).Length; i++)
                    {
                        images.Add(fd.SelectedPath + '/' + Path.GetFileName(Directory.GetFiles(fd.SelectedPath)[i]));
                    }
                }
                LoadImagePath.Text = fd.SelectedPath.ToString();
            }
            else
            {
                for (int i = 0; i < Directory.GetFiles(@"D:\Git Project\ocr-high-speed-detection\OK Image").Length; i++)
                {
                    images.Add(Path.GetFileName(Directory.GetFiles(@"D:\Git Project\ocr-high-speed-detection\OK Image")[i]));
                }
            }
        }
        //Run WorkSpace 1 Time
        private void RunOnceBT_Click(object sender, EventArgs e)
        {
            runWorkspace();
        }

        private void ContinuousCB_CheckedChanged(object sender, EventArgs e)
        {
            if (ContinuousCB.Checked)
            {
                RunOnceBT.Enabled = false;
                timer1.Start();
            }
            else
            {
                RunOnceBT.Enabled = true;
                timer1.Stop();
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            runWorkspace();
        }
    
        private void runWorkspace()
        {
            try
            {
                if(index > images.Count()-1)
                {
                    index = 0;
                }
                using (IImage image = new LibraryImage(images[index]))
                {
                        Stopwatch.Start();
                        sampleViewer.Sample = stream.Process(image);
                        Stopwatch.Stop();
                        IBlueMarking BlueMarking = stream.Process(image).Markings[sampleViewer.ToolName] as IBlueMarking;
                       //Count Number of OCR
                        label3.Text = BlueMarking.Views[0].Features.Count().ToString();
                        ProcessingText.Text = (Stopwatch.ElapsedMilliseconds).ToString() + " msec";

                        if ((Stopwatch.ElapsedMilliseconds) >= 35)
                        {
                            count = count + 1;
                            OverPS.Text = count.ToString();
                       }
                    
                    
                    index = index + 1;
                    totalrun = totalrun + 1;
                    Stopwatch.Reset();
                    label6.Text = totalrun.ToString();
                }
            }
            catch (InvalidCastException e)
            {
                MessageBox.Show("Can't Load Image Files");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["ConnectionString_DB"].ConnectionString;
            SqlConnection connection = new SqlConnection(constr);
            try
            {
                connection.Open();
            } catch
            {

            }
        }
    }
}
