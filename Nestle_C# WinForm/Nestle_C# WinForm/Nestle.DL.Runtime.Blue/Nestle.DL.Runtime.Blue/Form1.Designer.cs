﻿namespace Nestle.DL.Runtime.Blue
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.LoadImagePath = new System.Windows.Forms.TextBox();
            this.LoadImageBT = new System.Windows.Forms.Button();
            this.RunOnceBT = new System.Windows.Forms.Button();
            this.ProcessingText = new System.Windows.Forms.Label();
            this.ContinuousCB = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OverPS = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(40, 96);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(659, 200);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // LoadImagePath
            // 
            this.LoadImagePath.Location = new System.Drawing.Point(16, 408);
            this.LoadImagePath.Name = "LoadImagePath";
            this.LoadImagePath.Size = new System.Drawing.Size(664, 20);
            this.LoadImagePath.TabIndex = 1;
            // 
            // LoadImageBT
            // 
            this.LoadImageBT.Location = new System.Drawing.Point(696, 408);
            this.LoadImageBT.Name = "LoadImageBT";
            this.LoadImageBT.Size = new System.Drawing.Size(88, 23);
            this.LoadImageBT.TabIndex = 2;
            this.LoadImageBT.Text = "LoadImage";
            this.LoadImageBT.UseVisualStyleBackColor = true;
            this.LoadImageBT.Click += new System.EventHandler(this.LoadImageBT_Click);
            // 
            // RunOnceBT
            // 
            this.RunOnceBT.Location = new System.Drawing.Point(680, 16);
            this.RunOnceBT.Name = "RunOnceBT";
            this.RunOnceBT.Size = new System.Drawing.Size(104, 40);
            this.RunOnceBT.TabIndex = 3;
            this.RunOnceBT.Text = "Run Once";
            this.RunOnceBT.UseVisualStyleBackColor = true;
            this.RunOnceBT.Click += new System.EventHandler(this.RunOnceBT_Click);
            // 
            // ProcessingText
            // 
            this.ProcessingText.AutoSize = true;
            this.ProcessingText.Location = new System.Drawing.Point(736, 352);
            this.ProcessingText.Name = "ProcessingText";
            this.ProcessingText.Size = new System.Drawing.Size(24, 13);
            this.ProcessingText.TabIndex = 4;
            this.ProcessingText.Text = "n/a";
            // 
            // ContinuousCB
            // 
            this.ContinuousCB.AutoSize = true;
            this.ContinuousCB.Location = new System.Drawing.Point(680, 72);
            this.ContinuousCB.Name = "ContinuousCB";
            this.ContinuousCB.Size = new System.Drawing.Size(79, 17);
            this.ContinuousCB.TabIndex = 5;
            this.ContinuousCB.Text = "Continuous";
            this.ContinuousCB.UseVisualStyleBackColor = true;
            this.ContinuousCB.CheckedChanged += new System.EventHandler(this.ContinuousCB_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(648, 352);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Processing Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(608, 376);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Processing Time > 35 ms ";
            // 
            // OverPS
            // 
            this.OverPS.AutoSize = true;
            this.OverPS.Location = new System.Drawing.Point(736, 376);
            this.OverPS.Name = "OverPS";
            this.OverPS.Size = new System.Drawing.Size(24, 13);
            this.OverPS.TabIndex = 8;
            this.OverPS.Text = "n/a";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 312);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "n/a";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 312);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Result Count:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(680, 328);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "TotalRun:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(736, 328);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "n/a";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.OverPS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ContinuousCB);
            this.Controls.Add(this.ProcessingText);
            this.Controls.Add(this.RunOnceBT);
            this.Controls.Add(this.LoadImageBT);
            this.Controls.Add(this.LoadImagePath);
            this.Controls.Add(this.elementHost1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Nestle C#";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.TextBox LoadImagePath;
        private System.Windows.Forms.Button LoadImageBT;
        private System.Windows.Forms.Button RunOnceBT;
        private System.Windows.Forms.Label ProcessingText;
        private System.Windows.Forms.CheckBox ContinuousCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label OverPS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

